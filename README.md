# Grundlagen der Programmierung für Nicht-Informatiker

Repository für den IT Fort- und Weiterbildungskurs an der Universität Zürich


## Zu beachten für Windows Benutzer

Um auf Windows Python ausführen zu können, müsst ihr zuerst den neusten Python Interpreter installieren. Dieser könnt ihr hier herunterladen: https://www.python.org/downloads/windows/

## PyCharm Edu

PyCharm Edu enthält verschiedenste Übungen, um Eure Python Programmierkenntnisse zu vertiefen.

PyCharm Edu ist gratis und kann hier heruntergeladen werden: https://www.jetbrains.com/pycharm-edu/whatsnew/


## PyCharm

PyCharm ist eine der weitverbreitetsten Entwicklungsumgebungen für Python. Die Community Edition ist gratis und kann hier heruntergeladen werden: https://www.jetbrains.com/pycharm/download/

Hilfe zur Installation und Nutzung von PyCharm findet ihr hier (in Englisch): https://www.jetbrains.com/pycharm/help/meet-pycharm.html

## Verschiedenes

Interessanter und lesenswerter Artikel von The Economist zum Thema Coding: http://www.economist.com/blogs/economist-explains/2015/09/economist-explains-3


# Additional exercises

## Exercise I

Ask for three sentences and print them according to their length (longest first, shortest in the end).

## Exercise II
Given a non-negative number represented as a list of digits,
add 1 to the number ( increment the number represented by the digits ).

Example:

If the input vector is  ```[1, 2, 3]```
the returned vector should be ```[1, 2, 4]```
as 123 + 1 = 124.


```
def plusOne(number_list):
    """
    :param numbers: List[int]
    :return: List[int]
    """
    # Implementation goes here
```

## Exercise III
Write a function that expects a list of integers and checks if the list can be divided into two parts where the sum of the left part equals the sum of the right part. Your function should return True or False.

[5, 6, 2, 3, 4, 6, 6] --> True, [5, 6, 2, 3] and [4, 6, 6]

[4, 5, 6, 7, 8] --> False


## Exercise IV

Create a function that converts a roman number (e.g. "MCMLIV") into an integer (in this case 1954). The function accepts a string as a parameter and returns an integer.

These are the symbols for roman numbers:

- M:    1000
- D: 500
- C: 100
- L: 50
- X: 10
- V: 5
- I: 1

```
def romanToInt(roman_number):
    """
    :param roman_number: str
    :return: int
    """
    # Implementation goes here
```